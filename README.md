# go.gin-filemanager

> 基于 go-gin 框架的上传文件存储管理


## 安装获取/使用方式

1. 获取(使用 `go get`)\
    `go get gitee.com/zinface/go.gin-filemanager`

2. 使用
    ```golang
    // 导入 (golps会自动处理)， 可能需要执行 go mod tidy，来更新 go.mod
    import "gitee.com/zinface/go.gin-filemanager/filemanager"
    
    // 创建管理器实例
    var fm *filemanager.FileManager
    var ext string // 像数据库一样存储着文件扩展名

    func init() {
        fm = filemanager.NewFileManager("./static/upload")
    }

    // 配置文件上传处理器
	r.POST("/upload", func(ctx *gin.Context) {
		var uuid = "ba1f2511fc30423bdbb183fe33f3dd0f"  // 生成的 UUID
		meta, err := fm.SaveUploadFile(ctx, "upload", uuid)
		if err != nil {
			ctx.String(http.StatusInternalServerError,
				"upload file error:", err.Error())
			return
		}

		fmt.Println("meta.Name:", meta.Name)
		fmt.Println("meta.Ext:", meta.Ext)
		fmt.Println("meta.Path:", meta.Path)
		fmt.Println("meta.Size:", meta.Size)
		fmt.Println("meta.UUID:", meta.UUID)

		ext = meta.Ext // 像记录到数据库中的文件扩展名
		ctx.String(http.StatusOK, "upload file successfullys")
	})

    // 配置文件删除
    r.GET("/delete/:uuid", func(ctx *gin.Context) {
		var uuid = ctx.Param("uuid")   // 请求参数中的 UUID
		if err := fm.DeleteFile(uuid + ext); err != nil {
			ctx.String(http.StatusInternalServerError, err.Error())
		}
		ctx.String(http.StatusOK,
			"delete file successfullys")
	})
    ```
3. 测试
    ```bash
    # 使用 curl 发请 POST 请求进行文件上传，上传 go.mod 文件
    curl -X POST http://localhost:9999/upload   \
        -H 'Content-Type: multipart/form-data'  \
        -F 'upload=@go.mod' 

    # 上传成功将会在 ./static/upload 目录下产生一个结构为 <UUID><.EXT> 的文件
    # ba1f2511fc30423bdbb183fe33f3dd0f.mod

    # 使用 curl 发起 GET 请求删除文件
    curl http://localhost:9999/delete/ba1f2511fc30423bdbb183fe33f3dd0f
    # 文件删除成功将会在 ./static/upload 目录下删除一个结构为 <UUID><.EXT> 的文件
    # ba1f2511fc30423bdbb183fe33f3dd0f.mod
    ```