package filemanager

import "path/filepath"

type FileMeta struct {
	Name string `json:"name"`
	Size int64  `json:"size"`
	Ext  string `json:"ext"`
	Path string `json:"path"`
	UUID string `json:"uuid"`
}

func NewFileMeta(name string, size int64, uuid string) FileMeta {
	return FileMeta{
		Name: name,
		Size: size,
		Ext:  filepath.Ext(name),
		Path: uuid + filepath.Ext(name),
		UUID: uuid,
	}
}
