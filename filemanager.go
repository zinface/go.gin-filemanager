package filemanager

import (
	"errors"
	"os"
	"path"

	"github.com/gin-gonic/gin"
)

// 文件管理器
type FileManager struct {
	BasePath string
}

// NewFileManager 创建管理器实例.
// 要求 path 参数
func NewFileManager(path string) *FileManager {
	os.MkdirAll(path, 0755)
	return &FileManager{
		BasePath: path,
	}
}

// GetFilePath 获取将要保存的路经.
// 要求 filename 参数
func (fm *FileManager) GetFilePath(filename string) string {
	return path.Join(fm.BasePath, filename)
}

// GetSize 获取文件大小.
// 要求 filename 参数
func (fm *FileManager) GetSize(filename string) (int64, error) {
	if filename == "" {
		return 0, errors.New("filename is empty")
	}

	info, err := os.Stat(fm.GetFilePath(filename))
	if err != nil {
		return 0, err
	}
	return info.Size(), nil
}

// Exists 获取文件是否存在.
// 要求 filename 参数
func (fm *FileManager) Exists(filename string) bool {
	var path = fm.GetFilePath(filename)
	_, err := os.Stat(path)
	if err != nil {
		return os.IsExist(err)
	}
	return true
}

// SaveUploadFile 保存文件，并生成 FileMeta.
// 要求 c/form/uuid 参数
func (fm *FileManager) SaveUploadFile(c *gin.Context, form string, uuid string) (FileMeta, error) {
	fileHeader, err := c.FormFile(form)
	if err != nil {
		return FileMeta{}, err
	}

	filename := fileHeader.Filename
	filesize := fileHeader.Size

	meta := NewFileMeta(filename, filesize, uuid)
	err = c.SaveUploadedFile(fileHeader, fm.GetFilePath(meta.Path))

	return meta, err
}

// DeleteFile 删除文件，并返回错误值.
// 要求 filename 参数
func (fm *FileManager) DeleteFile(filename string) error {

	if !fm.Exists(filename) {
		return nil
	}

	return os.Remove(fm.GetFilePath(filename))
}
