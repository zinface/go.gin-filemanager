package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	filemanager "gitee.com/zinface/go.gin-filemanager"
)

var fm *filemanager.FileManager
var ext string

func init() {
	fm = filemanager.NewFileManager("./static/upload")
}

func main() {
	r := gin.Default()

	r.POST("/upload", func(ctx *gin.Context) {
		var uuid = "ba1f2511fc30423bdbb183fe33f3dd0f"
		meta, err := fm.SaveUploadFile(ctx, "upload", uuid)
		if err != nil {
			ctx.String(http.StatusInternalServerError,
				"upload file error:", err.Error())
			return
		}

		fmt.Println("meta.Name:", meta.Name)
		fmt.Println("meta.Ext:", meta.Ext)
		fmt.Println("meta.Path:", meta.Path)
		fmt.Println("meta.Size:", meta.Size)
		fmt.Println("meta.UUID:", meta.UUID)

		ext = meta.Ext
		ctx.String(http.StatusOK, "upload file successfullys")
	})

	r.GET("/delete/:uuid", func(ctx *gin.Context) {
		var uuid = ctx.Param("uuid")
		if err := fm.DeleteFile(uuid + ext); err != nil {
			ctx.String(http.StatusInternalServerError, err.Error())
		}
		ctx.String(http.StatusOK,
			"delete file successfullys")
	})

	r.Run(":9999")
}

// curl -X POST http://localhost:9999/upload -F 'upload=@go.mod' -H 'Content-Type: multipart/form-data'
// curl http://localhost:9999/delete/ba1f2511fc30423bdbb183fe33f3dd0f
